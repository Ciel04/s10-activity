package com.zuitt.discussion;

public class Post {
    // Properties
    private String title;

    //Constructors
    public Post(){

    }
    public Post(String title, String content){
        this.title = title;
    }
    //Getters
    public String getTitle(){
        return title;
    }
}
