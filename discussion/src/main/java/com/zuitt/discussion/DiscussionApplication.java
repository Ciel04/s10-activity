package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//@RequestMapping(value = "/posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts(){
		return "All posts retrieved";
	}

	//@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost(){
		return "New post created";
	}
	//Reteiving a post
	//@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}
	//Deleting a post
	//@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted";
	}
	//Updating a post
	//@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
	@ResponseBody
	//ResponseBody annotation allows the response to be sent back to the client in JSON format
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	//Retrieving post of  a particular user
	//@RequestMapping(value = "myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Post for "+user+ " has been retrieved.";
	}
	// So @GetMapping, @PostMapping, @PutMapping, @DeleteMapping annotations are called composed annotations which already provide the method to be used for a specific route

	//ACTIVITY S10
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}
	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@RequestHeader(value = "Authorization") String user,@PathVariable Long userid){
		String msg="";
		if(user!=""){
			msg = "The user " + userid + " has been deleted";
			return msg;
		}else {
			msg = "Unauthorized access";
			return msg;
		}
	}
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}


}
